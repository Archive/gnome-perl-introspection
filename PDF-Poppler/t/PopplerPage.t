#!/usr/bin/perl
use strict;
use warnings;
use Cwd qw(cwd);
use Glib qw(TRUE FALSE);
use Gtk2 -init;
use PDF::Poppler;
use Test::More tests => 16;

my $cwd = cwd();
my $uri = "file://$cwd/t/test.pdf";
my $document = PDF::Poppler::Document -> new_from_file($uri);

my $page = $document -> get_page(0);

my ($width, $height) = $page -> get_size();
is($width, 595.28);
is($height, 841.89);

my $pixbuf = Gtk2::Gdk::Pixbuf -> new("rgb", TRUE, 8, $width, $height);
$page -> render_to_pixbuf(0, 0, $width, $height, 1.0, 0, $pixbuf);

is($page -> get_index(), 0);
is($page -> get_thumbnail(), undef);
is_deeply([$page -> get_thumbnail_size()], []);

my @rectangles = $page -> find_text("tutorial");
is(@rectangles, 1);
isa_ok($rectangles[0], "PDF::Poppler::Rectangle");
isa_ok($rectangles[0], "Glib::Boxed");

like($rectangles[0] -> x1(), qr/^[\d.]+$/);
like($rectangles[0] -> y1(), qr/^[\d.]+$/);
like($rectangles[0] -> x2(), qr/^[\d.]+$/);
like($rectangles[0] -> y2(), qr/^[\d.]+$/);

TODO: {
  local $TODO = "should this work?";
  is($page -> get_text('word', $rectangles[0]), "tutorial");
}

my @mappings = $page -> get_link_mapping();
is(@mappings, 1);
isa_ok($mappings[0], "PDF::Poppler::LinkMapping");

my @regions = $page -> get_selection_region(1.0, 'line', $rectangles[0]);
SKIP: {
  skip 'no selection regions', 1
    unless scalar @regions;
  isa_ok($regions[0], "Gtk2::Gdk::Region");
}

my $blue = Gtk2::Gdk::Color -> new(0, 65535, 0);
$page -> render_selection_to_pixbuf(
  1.0, 0, $pixbuf,
  $rectangles[0], $rectangles[0],
  'line',
  $blue, $blue);
