#!/usr/bin/perl
use strict;
use warnings;
use Glib qw(TRUE FALSE);
use Gtk2 -init;
use PDF::Poppler;
use Test::More tests => 6;

my $rectangle = PDF::Poppler::Rectangle -> new();
isa_ok($rectangle, "PDF::Poppler::Rectangle");
isa_ok($rectangle, "Glib::Boxed");

$rectangle -> x1(1.234);
$rectangle -> y1(2.134);
$rectangle -> x2(3.124);
$rectangle -> y2(4.123);

is($rectangle -> x1(), 1.234);
is($rectangle -> y1(), 2.134);
is($rectangle -> x2(), 3.124);
is($rectangle -> y2(), 4.123);
