#!/usr/bin/perl
use strict;
use warnings;
use Test::More tests => 7;

BEGIN { use_ok("PDF::Poppler"); }

ok(defined PDF::Poppler::get_backend());
ok(defined PDF::Poppler -> get_backend());
ok(PDF::Poppler::get_backend() eq PDF::Poppler -> get_backend());

ok(defined PDF::Poppler::get_version());
ok(defined PDF::Poppler -> get_version());
ok(PDF::Poppler::get_version() eq PDF::Poppler -> get_version());
