#!/usr/bin/perl
use strict;
use warnings;
use Cwd qw(cwd);
use Glib qw(TRUE FALSE);
use PDF::Poppler;
use Test::More tests => 2;

my $cwd = cwd();
my $uri = "file://$cwd/t/test.pdf";
my $document = PDF::Poppler::Document -> new_from_file($uri);

my $iter = PDF::Poppler::IndexIter -> new($document);

SKIP: {
  skip "outline tests", 2
    unless defined $iter;

  isa_ok($iter, "PDF::Poppler::IndexIter");
  isa_ok($iter, "Glib::Boxed");

  # get_child
  # is_open
  # get_action
  # next
}
