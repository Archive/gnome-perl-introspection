#!/usr/bin/perl
use strict;
use warnings;
use Cwd qw(cwd);
use Glib qw(TRUE FALSE);
use PDF::Poppler;
use Test::More tests => 15;

eval {
  PDF::Poppler::Document -> new_from_file("urgs/urgs");
};

isa_ok($@, "Glib::Convert::Error");
$@ = undef;

my $cwd = cwd();
my $uri = "file://$cwd/t/test.pdf";

my $document = PDF::Poppler::Document -> new_from_file($uri);
isa_ok($document, "PDF::Poppler::Document");
isa_ok($document, "Glib::Object");

$document = PDF::Poppler::Document -> new_from_file($uri, "urgs");
isa_ok($document, "PDF::Poppler::Document");
isa_ok($document, "Glib::Object");

is($document -> get_n_pages(), 2);

my $page = $document -> get_page(0);
isa_ok($page, "PDF::Poppler::Page");
is($page -> get_index(), 0);

$page = $document -> get_page_by_label("2");
isa_ok($page, "PDF::Poppler::Page");
is($page -> get_index(), 1);

is($document -> save($uri . ".bak"), TRUE);
unlink "$cwd/t/test.pdf.bak";

eval {
  is($document -> save("http://www.google.com"), FALSE);
};

isa_ok($@, "Glib::Convert::Error");
$@ = undef;

ok(!$document -> has_attachments());
my @attachments = $document -> get_attachments();
is(scalar @attachments, 0);

is($document -> find_dest("http://www.fpdf.org"), undef);
