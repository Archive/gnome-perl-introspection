#!/usr/bin/perl
use strict;
use warnings;
use Glib qw(TRUE FALSE);
use Gtk2 -init;
use PDF::Poppler;
use Test::More tests => 8;

my $mapping = PDF::Poppler::LinkMapping -> new();
isa_ok($mapping, "PDF::Poppler::LinkMapping");
isa_ok($mapping, "Glib::Boxed");

isa_ok(my $rectangle = $mapping -> area(), "PDF::Poppler::Rectangle");

{
  $rectangle -> x1(1.234);
  $rectangle -> y1(2.134);
  $rectangle -> x2(3.124);
  $rectangle -> y2(4.123);

  $mapping -> area($rectangle);
  is($mapping -> area() -> x1(), 1.234);
  is($mapping -> area() -> y1(), 2.134);
  is($mapping -> area() -> x2(), 3.124);
  is($mapping -> area() -> y2(), 4.123);
}

{
  isa_ok(my $action = $mapping -> action(), "PDF::Poppler::Action");
  # FIXME:
  #warn $action -> type();
  #warn $action -> any();
}
