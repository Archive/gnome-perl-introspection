package PDF::Poppler;

=head1 NAME

PDF::Poppler - Perl interface to the Poppler library

=head1 ABSTRACT

The great new PDF::Poppler!

=head1 SYNOPSIS

  use Cwd qw(abs_path);
  use Glib qw(TRUE FALSE filename_to_uri);
  use Gtk2 -init;
  use PDF::Poppler;

  my ($width, $height) = (600, 800);

  my $uri = filename_to_uri(abs_path($ARGV[0]), undef);
  my $document = PDF::Poppler::Document -> new_from_file($uri);

  my $pixbuf = Gtk2::Gdk::Pixbuf -> new("rgb", TRUE, 8, $width, $height);
  my $page = $document -> get_page(0);
  $page -> render_to_pixbuf(0, 0, $width, $height, 1.0, $pixbuf);

  my $window = Gtk2::Window -> new();
  my $image = Gtk2::Image -> new_from_pixbuf($pixbuf);

  $window -> signal_connect(delete_event => sub { Gtk2 -> main_quit(); });
  $window -> add($image);
  $window -> show_all();

  Gtk2 -> main();

=head1 AUTHOR

=over

=item Torsten Schoenfeld, E<lt>kaffeetisch at gmx dot deE<gt>

=back

=head1 COPYRIGHT & LICENSE

Copyright (C) 2005 by the gtk2-perl team

This program is free software; you can redistribute it and/or modify it under
the terms of the LGPL.

=cut

# --------------------------------------------------------------------------- #

use warnings;
use strict;

use Glib::Object::Introspection;
use Gtk2;

use base qw(Glib::Object::Introspection);

our $VERSION = '0.01';

sub import {
  Glib::Object::Introspection -> setup(Poppler => '0.8', 'PDF::Poppler');
}

# --------------------------------------------------------------------------- #

package PDF::Poppler::Document;

sub get_attachments {
  $Glib::Object::Introspection::AUTOLOAD =
    "PDF::Poppler::Document::get_attachments";
  my $ref = Glib::Object::Introspection::AUTOLOAD(@_);

  if (wantarray()) {
    return defined $ref ? @$ref : ();
  } else {
    return defined $ref ? $ref->[-1] : undef;
  }
}

package PDF::Poppler::Page;

sub get_thumbnail_size {
  $Glib::Object::Introspection::AUTOLOAD =
    "PDF::Poppler::Page::get_thumbnail_size";
  my ($bool, $w, $h) = Glib::Object::Introspection::AUTOLOAD(@_);

  return $bool ? ($w, $h) : ();
}

sub find_text {
  $Glib::Object::Introspection::AUTOLOAD =
    "PDF::Poppler::Page::find_text";
  my $ref = Glib::Object::Introspection::AUTOLOAD(@_);

  if (wantarray()) {
    return defined $ref ? @$ref : ();
  } else {
    return defined $ref ? $ref->[-1] : undef;
  }
}

sub get_link_mapping {
  $Glib::Object::Introspection::AUTOLOAD =
    "PDF::Poppler::Page::get_link_mapping";
  my $ref = Glib::Object::Introspection::AUTOLOAD(@_);

  if (wantarray()) {
    return defined $ref ? @$ref : ();
  } else {
    return defined $ref ? $ref->[-1] : undef;
  }
}

sub get_selection_region {
  my $ref = Glib::Object::Introspection::invoke(
    "PDF::Poppler::Page::get_selection_region", @_);

  if (wantarray()) {
    return defined $ref ? @$ref : ();
  } else {
    return defined $ref ? $ref->[-1] : undef;
  }
}

1;
