#!/usr/bin/perl
use strict;
use warnings;
use Cwd qw(abs_path);
use Glib qw(TRUE FALSE filename_to_uri);
use Gtk2 -init;
use PDF::Poppler;

our $CURRENT = 0;

my $uri = filename_to_uri(abs_path($ARGV[0]), undef);
my $document = PDF::Poppler::Document -> new_from_file($uri);

my $window = Gtk2::Window -> new();
my $box = Gtk2::VBox -> new(FALSE, 0);
my $button_box = Gtk2::HButtonBox -> new();

my $button_prev = Gtk2::Button -> new_from_stock("gtk-go-back");
$button_prev -> signal_connect(clicked => sub {
  if ($CURRENT > 0) {
    show_page(--$CURRENT);
  }

  return TRUE;
});

my $button_next = Gtk2::Button -> new_from_stock("gtk-go-forward");
$button_next -> signal_connect(clicked => sub {
  if ($CURRENT < ($document -> get_n_pages() - 1)) {
    show_page(++$CURRENT);
  }

  return TRUE;
});

$button_box -> pack_start($button_prev, FALSE, FALSE, 5);
$button_box -> pack_start($button_next, FALSE, FALSE, 5);

my ($width, $height) = (600, 800);
my $pixbuf = Gtk2::Gdk::Pixbuf -> new("rgb", TRUE, 8, $width, $height);

my $image = Gtk2::Image -> new_from_pixbuf($pixbuf);

$box -> pack_start($button_box, FALSE, FALSE, 5);
$box -> pack_start($image, TRUE, TRUE, 5);
$box -> set_border_width(5);

$window -> add($box);
$window -> show_all();

show_page($CURRENT);

$window -> signal_connect(delete_event => sub { Gtk2 -> main_quit(); });

Gtk2 -> main();

sub show_page {
  my ($index) = @_;

  my $page = $document -> get_page($index);
  $page -> render_to_pixbuf(0, 0, $width, $height, 1.0, 0.0, $pixbuf);
  $image -> set_from_pixbuf($pixbuf);

  $button_prev -> set_sensitive($index > 0);
  $button_next -> set_sensitive($index < ($document -> get_n_pages() - 1));
}
