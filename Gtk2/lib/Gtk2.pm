package Gtk2;

use warnings;
use strict;

use Glib::Object::Introspection;
use Glib;

use base qw(Glib::Object::Introspection);

our $VERSION = '0.01';

sub import {
  warn "here";

  my $filename = "Gtk2/libgtk-x11-2.0-i11n.so";
  my $full_filename = undef;

  foreach my $prefix (@INC) {
    my $tmp = "$prefix/$filename";
    if (-f $tmp) {
      $full_filename = $tmp;
      last;
    }
  }

  die "Can't find $filename in \@INC" unless defined $full_filename;

  Glib::Object::Introspection -> setup("libgtk-x11-2.0.so",
                                       $full_filename,
                                       "Gtk", "Gtk2");
}

1;
