package Glib::Object::Introspection;

# $Id: Introspection.pm,v 1.3 2005/08/26 18:47:42 torsten Exp $

use 5.008;
use strict;
use warnings;

use Carp qw(croak);
use Glib;

require DynaLoader;

our @ISA = qw(DynaLoader);
our $VERSION = '0.01';

sub dl_load_flags { $^O eq 'darwin' ? 0x00 : 0x01 }

Glib::Object::Introspection -> bootstrap($VERSION);

# --------------------------------------------------------------------------- #

our $AUTOLOAD;
our $REPOSITORY;
our $BASENAME;
our $PACKAGE;

sub setup {
  my ($self, $basename, $version, $package) = @_;

  $BASENAME = $basename;
  $PACKAGE = $package;
  $REPOSITORY = Glib::Object::Introspection::Repository -> get_default();

  $REPOSITORY -> require($basename, $version, []);
  $REPOSITORY -> register_types($basename => $package);
}

sub AUTOLOAD {
  invoke($AUTOLOAD, @_);
}

sub invoke {
  my $full_symbol = shift;
  (my $symbol = $full_symbol) =~ s/${PACKAGE}:://;

  # namespace::method
  if ($symbol =~ m/(.+)::(.+)/) {
    my ($namespace, $method) = ($1, $2);

    my $object = $REPOSITORY -> find_by_name($BASENAME, $namespace);

    if (my $info = $object -> find_method($method)) {
      return $info -> invoke(@_);
    }

    elsif (($info) = grep { $_ -> get_name() eq $method }
                          $object -> get_fields()) {
      if (@_ != 1 && @_ != 2) {
        croak "Invalid invocation of a field accessor";
      }

      return defined $_[1]
        ? $object -> access_field($info, $_[0], $_[1])
        : $object -> access_field($info, $_[0]);
    }

    else {
      croak "Dispatch error: ${PACKAGE}::$symbol doesn't exist";
    }
  }

  # function
  else {
    my $info = $REPOSITORY -> find_by_name($BASENAME, $symbol);
    if ($info) {
      return $info -> invoke(@_);
    }
  }

  croak "Invalid invocation: Cannot handle $full_symbol";
}

1;

# --------------------------------------------------------------------------- #

__END__

=head1 NAME

Glib::Object::Introspection - Perl bindings for GLib's introspection library

=head1 SYNOPSIS

  # ...

=head1 ABSTRACT

Automatically generate Perl bindings from vendor-supplied IDL files!

=head1 AUTHORS

=over

=item Torsten Schoenfeld, E<lt>kaffeetisch at gmx dot deE<gt>

=item muppet, E<lt>scott at asofyet dot orgE<gt>

=back

=head1 COPYRIGHT

Copyright (C) 2005 by the gtk2-perl team

=cut
