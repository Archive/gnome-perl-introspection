/*
 * Copyright (C) 2005 by the gtk2-perl team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: IdlInterfaceInfo.xs,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $
 */

#include "gobject-introspection-perl.h"

SV *
newSVGIInterfaceInfo (GIInterfaceInfo *info)
{
        SV *sv = newSV (0);

        return sv_setref_pv (sv, "Glib::Object::Introspection::InterfaceInfo", info);
}

GIInterfaceInfo *
SvGIInterfaceInfo (SV *info)
{
        return INT2PTR (GIInterfaceInfo *, SvIV (SvRV (info)));

}

MODULE = Glib::Object::Introspection::InterfaceInfo	PACKAGE = Glib::Object::Introspection::InterfaceInfo	PREFIX = g_interface_info_

# gint g_interface_info_get_n_prerequisites (GIInterfaceInfo *info);
# GIBaseInfo * g_interface_info_get_prerequisite (GIInterfaceInfo *info, gint n);
void
g_interface_info_get_prerequisites (info)
	GIInterfaceInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_interface_info_get_n_prerequisites (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIBaseInfo (
		  g_interface_info_get_prerequisite (info, i))));

# gint g_interface_info_get_n_properties (GIInterfaceInfo *info);
# GIPropertyInfo * g_interface_info_get_property (GIInterfaceInfo *info, gint n);
void
g_interface_info_get_properties (info)
	GIInterfaceInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_interface_info_get_n_properties (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIPropertyInfo (
		  g_interface_info_get_property (info, i))));

# gint g_interface_info_get_n_methods (GIInterfaceInfo *info);
# GIFunctionInfo * g_interface_info_get_method (GIInterfaceInfo *info, gint n);
void
g_interface_info_get_methods (info)
	GIInterfaceInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_interface_info_get_n_methods (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIFunctionInfo (
		  g_interface_info_get_method (info, i))));

GIFunctionInfo * g_interface_info_find_method (GIInterfaceInfo *info, const gchar *name);

# gint g_interface_info_get_n_signals (GIInterfaceInfo *info);
# GISignalInfo * g_interface_info_get_signal (GIInterfaceInfo *info, gint n);
void
g_interface_info_get_signals (info)
	GIInterfaceInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_interface_info_get_n_signals (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGISignalInfo (
		  g_interface_info_get_signal (info, i))));

# gint g_interface_info_get_n_vfuncs (GIInterfaceInfo *info);
# GIVFuncInfo * g_interface_info_get_vfunc (GIInterfaceInfo *info, gint n);
void
g_interface_info_get_vfuncs (info)
	GIInterfaceInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_interface_info_get_n_vfuncs (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIVFuncInfo (
		  g_interface_info_get_vfunc (info, i))));

# gint g_interface_info_get_n_constants (GIInterfaceInfo *info);
# GIConstantInfo * g_interface_info_get_constant (GIInterfaceInfo *info, gint n);
void
g_interface_info_get_constants (info)
	GIInterfaceInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_interface_info_get_n_constants (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIConstantInfo (
		  g_interface_info_get_constant (info, i))));
