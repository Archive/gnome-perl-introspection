/*
 * Copyright (C) 2005 by the gtk2-perl team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: IdlSignalInfo.xs,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $
 */

#include "gobject-introspection-perl.h"

SV *
newSVGISignalInfo (GISignalInfo *info)
{
        SV *sv = newSV (0);

        return sv_setref_pv (sv, "Glib::Object::Introspection::SignalInfo", info);
}

GISignalInfo *
SvGISignalInfo (SV *info)
{
        return INT2PTR (GISignalInfo *, SvIV (SvRV (info)));

}

MODULE = Glib::Object::Introspection::SignalInfo	PACKAGE = Glib::Object::Introspection::SignalInfo	PREFIX = g_signal_info_

GSignalFlags g_signal_info_get_flags (GISignalInfo *info);

GIVFuncInfo * g_signal_info_get_class_closure (GISignalInfo *info);

gboolean g_signal_info_true_stops_emit (GISignalInfo *info);
