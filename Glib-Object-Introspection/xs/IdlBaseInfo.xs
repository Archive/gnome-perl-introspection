/*
 * Copyright (C) 2005 by the gtk2-perl team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: IdlBaseInfo.xs,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $
 */

#include "gobject-introspection-perl.h"

SV *
newSVGIBaseInfo (GIBaseInfo *info)
{
        SV *sv = newSV (0);
	char *package = "Glib::Object::Introspection::BaseInfo";

	if (info) {
		switch (g_base_info_get_type (info)) {
			case GI_INFO_TYPE_INVALID:
				croak ("Invalid type?");
				break;
			case GI_INFO_TYPE_FUNCTION:
				package = "Glib::Object::Introspection::FunctionInfo";
				break;
			case GI_INFO_TYPE_CALLBACK:
				package = "Glib::Object::Introspection::CallbackInfo";
				break;
			case GI_INFO_TYPE_STRUCT:
				package = "Glib::Object::Introspection::StructInfo";
				break;
			case GI_INFO_TYPE_BOXED:
				package = "Glib::Object::Introspection::BoxedInfo";
				break;
			case GI_INFO_TYPE_ENUM:
				package = "Glib::Object::Introspection::EnumInfo";
				break;
			case GI_INFO_TYPE_FLAGS:
				package = "Glib::Object::Introspection::FlagsInfo";
				break;
			case GI_INFO_TYPE_OBJECT:
				package = "Glib::Object::Introspection::ObjectInfo";
				break;
			case GI_INFO_TYPE_INTERFACE:
				package = "Glib::Object::Introspection::InterfaceInfo";
				break;
			case GI_INFO_TYPE_CONSTANT:
				package = "Glib::Object::Introspection::ConstantInfo";
				break;
			case GI_INFO_TYPE_ERROR_DOMAIN:
				package = "Glib::Object::Introspection::ErrorDomainInfo";
				break;
			case GI_INFO_TYPE_UNION:
				package = "Glib::Object::Introspection::UnionInfo";
				break;
			case GI_INFO_TYPE_VALUE:
				package = "Glib::Object::Introspection::ValueInfo";
				break;
			case GI_INFO_TYPE_SIGNAL:
				package = "Glib::Object::Introspection::SignalInfo";
				break;
			case GI_INFO_TYPE_VFUNC:
				package = "Glib::Object::Introspection::VFuncInfo";
				break;
			case GI_INFO_TYPE_PROPERTY:
				package = "Glib::Object::Introspection::PropertyInfo";
				break;
			case GI_INFO_TYPE_FIELD:
				package = "Glib::Object::Introspection::FieldInfo";
				break;
			case GI_INFO_TYPE_ARG:
				package = "Glib::Object::Introspection::ArgInfo";
				break;
			case GI_INFO_TYPE_TYPE:
				package = "Glib::Object::Introspection::TypeInfo";
				break;
			case GI_INFO_TYPE_UNRESOLVED:
				croak ("Unresolved type?");
				break;
		}
	}

        return sv_setref_pv (sv, package, info);
}

GIBaseInfo *
SvGIBaseInfo (SV *info)
{
        return INT2PTR (GIBaseInfo *, SvIV (SvRV (info)));

}

MODULE = Glib::Object::Introspection::BaseInfo	PACKAGE = Glib::Object::Introspection::BaseInfo	PREFIX = g_base_info_

BOOT:
	gperl_set_isa ("Glib::Object::Introspection::ArgInfo", "Glib::Object::Introspection::BaseInfo");
	gperl_set_isa ("Glib::Object::Introspection::BoxedInfo", "Glib::Object::Introspection::StructInfo");
	gperl_set_isa ("Glib::Object::Introspection::CallableInfo", "Glib::Object::Introspection::BaseInfo");
	gperl_set_isa ("Glib::Object::Introspection::CallbackInfo", "Glib::Object::Introspection::CallableInfo");
	gperl_set_isa ("Glib::Object::Introspection::ConstantInfo", "Glib::Object::Introspection::BaseInfo");
	gperl_set_isa ("Glib::Object::Introspection::EnumInfo", "Glib::Object::Introspection::RegisteredTypeInfo");
	gperl_set_isa ("Glib::Object::Introspection::ErrorInfo", "Glib::Object::Introspection::BaseInfo");
	gperl_set_isa ("Glib::Object::Introspection::FieldInfo", "Glib::Object::Introspection::BaseInfo");
	gperl_set_isa ("Glib::Object::Introspection::FlagsInfo", "Glib::Object::Introspection::EnumInfo");
	gperl_set_isa ("Glib::Object::Introspection::FunctionInfo", "Glib::Object::Introspection::CallableInfo");
	gperl_set_isa ("Glib::Object::Introspection::InterfaceInfo", "Glib::Object::Introspection::RegisteredTypeInfo");
	gperl_set_isa ("Glib::Object::Introspection::ObjectInfo", "Glib::Object::Introspection::RegisteredTypeInfo");
	gperl_set_isa ("Glib::Object::Introspection::PropertyInfo", "Glib::Object::Introspection::BaseInfo");
	gperl_set_isa ("Glib::Object::Introspection::RegisteredTypeInfo", "Glib::Object::Introspection::BaseInfo");
	gperl_set_isa ("Glib::Object::Introspection::SignalInfo", "Glib::Object::Introspection::CallableInfo");
	gperl_set_isa ("Glib::Object::Introspection::StructInfo", "Glib::Object::Introspection::RegisteredTypeInfo");
	gperl_set_isa ("Glib::Object::Introspection::TypeInfo", "Glib::Object::Introspection::BaseInfo");
	gperl_set_isa ("Glib::Object::Introspection::UnionInfo", "Glib::Object::Introspection::RegisteredTypeInfo");
	gperl_set_isa ("Glib::Object::Introspection::VFuncInfo", "Glib::Object::Introspection::CallableInfo");
	gperl_set_isa ("Glib::Object::Introspection::ValueInfo", "Glib::Object::Introspection::BaseInfo");

void
DESTROY (info)
	GIBaseInfo *info
    CODE:
	g_base_info_unref (info);

GIInfoType g_base_info_get_type (GIBaseInfo *info);

const gchar * g_base_info_get_name (GIBaseInfo *info);

const gchar * g_base_info_get_namespace (GIBaseInfo *info);

gboolean g_base_info_is_deprecated (GIBaseInfo *info);

const gchar * g_base_info_get_annotation (GIBaseInfo *info, const gchar  *name);

GIBaseInfo * g_base_info_get_container (GIBaseInfo *info);
    CLEANUP:
	/* FIXME: should this be necessary? */
	g_base_info_ref (info);

# FIXME: Needed?
# GIBaseInfo * g_info_new (GIInfoType type, GIBaseInfo *container, const guchar *metadata, guint32 offset);
