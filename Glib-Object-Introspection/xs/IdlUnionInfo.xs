/*
 * Copyright (C) 2005 by the gtk2-perl team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: IdlUnionInfo.xs,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $
 */

#include "gobject-introspection-perl.h"
#include "gobject-introspection-perl-private.h"

SV *
newSVGIUnionInfo (GIUnionInfo *info)
{
        SV *sv = newSV (0);

        return sv_setref_pv (sv, "Glib::Object::Introspection::UnionInfo", info);
}

GIUnionInfo *
SvGIUnionInfo (SV *info)
{
        return INT2PTR (GIUnionInfo *, SvIV (SvRV (info)));

}

#define ACCESS_VALUE(type, to, fro)									\
	{												\
		type tmp;										\
		if (g_field_info_get_flags (field_info) & GI_FIELD_IS_READABLE) {			\
			memcpy (&tmp, pointer, sizeof (type));						\
			RETVAL = to (tmp);								\
		}											\
		if (value) {										\
			if (g_field_info_get_flags (field_info) & GI_FIELD_IS_WRITABLE) {		\
				tmp = fro (value);							\
				memcpy (pointer, &tmp, sizeof (type));					\
			} else {									\
				croak ("Field %s is not writable",					\
				       g_base_info_get_name ((GIBaseInfo*) field_info));		\
			}										\
		}											\
	}

MODULE = Glib::Object::Introspection::UnionInfo	PACKAGE = Glib::Object::Introspection::UnionInfo	PREFIX = g_union_info_

# gint g_union_info_get_n_fields (GIUnionInfo *info);
# GIFieldInfo * g_union_info_get_field (GIUnionInfo *info, gint n);
void
g_union_info_get_fields (info)
	GIUnionInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_union_info_get_n_fields (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIFieldInfo (
		  g_union_info_get_field (info, i))));

# gint g_union_info_get_n_methods (GIUnionInfo *info);
# GIFunctionInfo * g_union_info_get_method (GIUnionInfo *info, gint n);

# gboolean g_union_info_is_discriminated (GIUnionInfo *info);

# gint g_union_info_get_discriminator_offset (GIUnionInfo *info);

# GITypeInfo * g_union_info_get_discriminator_type (GIUnionInfo *info);

# GIConstantInfo * g_union_info_get_discriminator (GIUnionInfo *info, gint n);

GIFunctionInfo * g_union_info_find_method (GIUnionInfo *info, const gchar *name);

SV *
access_field (info, field_info, object, value=NULL)
	GIBaseInfo *info
	GIFieldInfo *field_info
	SV *object
	SV *value
    PREINIT:
	GType type;
	void *pointer;
	GITypeInfo *field_type_info;
	GITypeTag tag;
    CODE:
	warn ("%p", g_union_info_get_discriminator ((GIUnionInfo *) info, 0));
	type = gperl_i11n_get_gtype ((GIRegisteredTypeInfo *) info);
	pointer = gperl_get_boxed_check (object, type);

	field_type_info = g_field_info_get_type (field_info);
	tag = g_type_info_get_tag (field_type_info);

	RETVAL = &PL_sv_undef;

	switch (tag) {
	    case GI_TYPE_TAG_VOID:
		break;

	    case GI_TYPE_TAG_BOOLEAN:
		ACCESS_VALUE (gboolean, newSVuv, SvUV);
		break;

	    case GI_TYPE_TAG_INT8:
		ACCESS_VALUE (gint8, newSViv, SvIV);
		break;

	    case GI_TYPE_TAG_UINT8:
		ACCESS_VALUE (guint8, newSVuv, SvUV);
		break;

	    case GI_TYPE_TAG_INT16:
		ACCESS_VALUE (gint16, newSViv, SvIV);
		break;

	    case GI_TYPE_TAG_UINT16:
		ACCESS_VALUE (guint16, newSVuv, SvUV);
		break;

	    case GI_TYPE_TAG_INT32:
		ACCESS_VALUE (gint32, newSViv, SvIV);
		break;

	    case GI_TYPE_TAG_UINT32:
		ACCESS_VALUE (guint32, newSVuv, SvUV);
		break;

	    case GI_TYPE_TAG_INT64:
	    case GI_TYPE_TAG_UINT64:
		croak ("FIXME - 64bit types");
		break;

	    case GI_TYPE_TAG_INT:
		ACCESS_VALUE (gint, newSViv, SvIV);
		break;

	    case GI_TYPE_TAG_UINT:
		ACCESS_VALUE (guint, newSVuv, SvUV);
		break;

	    case GI_TYPE_TAG_LONG:
		ACCESS_VALUE (glong, newSVnv, SvNV);
		break;

	    case GI_TYPE_TAG_ULONG:
		ACCESS_VALUE (gulong, newSVnv, SvNV);
		break;

	    case GI_TYPE_TAG_SSIZE:
	    case GI_TYPE_TAG_SIZE:
		croak ("FIXME - size types");
		break;

	    case GI_TYPE_TAG_FLOAT:
		ACCESS_VALUE (gfloat, newSVnv, SvNV);
		break;

	    case GI_TYPE_TAG_DOUBLE:
		ACCESS_VALUE (gdouble, newSVnv, SvNV);
		break;

	    case GI_TYPE_TAG_UTF8:
		ACCESS_VALUE (gchar*, newSVGChar, SvGChar);
		break;

	    case GI_TYPE_TAG_FILENAME:
		ACCESS_VALUE (gchar*, gperl_sv_from_filename, gperl_filename_from_sv);
		break;

	    case GI_TYPE_TAG_INTERFACE:
	    {
		if (g_field_info_get_flags (field_info) & GI_FIELD_IS_READABLE) {
			RETVAL = gperl_i11n_pointer_to_sv (field_type_info, pointer, FALSE);
		}
		if (value) {
			if (g_field_info_get_flags (field_info) & GI_FIELD_IS_WRITABLE) {
				memcpy (pointer, gperl_i11n_sv_to_pointer (field_type_info, value), sizeof (void*));
			} else {
				croak ("Field %s is not writable",
				       g_base_info_get_name ((GIBaseInfo*) field_info));
			}
		}
	    }
		break;

	    case GI_TYPE_TAG_ARRAY:
	    case GI_TYPE_TAG_GLIST:
	    case GI_TYPE_TAG_GSLIST:
	    case GI_TYPE_TAG_GHASH:
	    case GI_TYPE_TAG_ERROR:
		croak ("FIXME - array, list, hash, error types");
		break;

	    default:
		croak ("Unhandled type tag: %d", tag);
	}

	g_base_info_unref ((GIBaseInfo *) field_type_info);
    OUTPUT:
	RETVAL
