/*
 * Copyright (C) 2005 by the gtk2-perl team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: IdlCallableInfo.xs,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $
 */

#include "gobject-introspection-perl.h"

SV *
newSVGICallableInfo (GICallableInfo *info)
{
        SV *sv = newSV (0);

        return sv_setref_pv (sv, "Glib::Object::Introspection::CallableInfo", info);
}

GICallableInfo *
SvGICallableInfo (SV *info)
{
        return INT2PTR (GICallableInfo *, SvIV (SvRV (info)));

}

MODULE = Glib::Object::Introspection::CallableInfo	PACKAGE = Glib::Object::Introspection::CallableInfo	PREFIX = g_callable_info_

GITypeInfo * g_callable_info_get_return_type (GICallableInfo *info);

GITransfer g_callable_info_get_caller_owns (GICallableInfo *info);

gboolean g_callable_info_may_return_null (GICallableInfo *info);

# gint g_callable_info_get_n_args (GICallableInfo *info);
# GIArgInfo * g_callable_info_get_arg (GICallableInfo *info, gint n);
void
g_callable_info_get_args (info)
	GICallableInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_callable_info_get_n_args (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIArgInfo (
		  g_callable_info_get_arg (info, i))));
