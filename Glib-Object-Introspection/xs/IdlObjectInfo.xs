/*
 * Copyright (C) 2005 by the gtk2-perl team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: IdlObjectInfo.xs,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $
 */

#include "gobject-introspection-perl.h"

SV *
newSVGIObjectInfo (GIObjectInfo *info)
{
        SV *sv = newSV (0);

        return sv_setref_pv (sv, "Glib::Object::Introspection::ObjectInfo", info);
}

GIObjectInfo *
SvGIObjectInfo (SV *info)
{
        return INT2PTR (GIObjectInfo *, SvIV (SvRV (info)));

}

MODULE = Glib::Object::Introspection::ObjectInfo	PACKAGE = Glib::Object::Introspection::ObjectInfo	PREFIX = g_object_info_

GIObjectInfo * g_object_info_get_parent (GIObjectInfo *info);

# gint g_object_info_get_n_interfaces (GIObjectInfo *info);
# GIInterfaceInfo * g_object_info_get_interface (GIObjectInfo *info, gint n);
void
g_object_info_get_interfaces (info)
	GIObjectInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_object_info_get_n_interfaces (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIInterfaceInfo (
		  g_object_info_get_interface (info, i))));

# gint g_object_info_get_n_fields (GIObjectInfo *info);
# GIFieldInfo * g_object_info_get_field (GIObjectInfo *info, gint n);
void
g_object_info_get_fields (info)
	GIObjectInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_object_info_get_n_fields (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIFieldInfo (
		  g_object_info_get_field (info, i))));

# gint g_object_info_get_n_properties (GIObjectInfo *info);
# GIPropertyInfo * g_object_info_get_property (GIObjectInfo *info, gint n);
void
g_object_info_get_properties (info)
	GIObjectInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_object_info_get_n_properties (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIPropertyInfo (
		  g_object_info_get_property (info, i))));

# gint g_object_info_get_n_methods (GIObjectInfo *info);
# GIFunctionInfo * g_object_info_get_method (GIObjectInfo *info, gint n);
void
g_object_info_get_methods (info)
	GIObjectInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_object_info_get_n_methods (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIFunctionInfo (
		  g_object_info_get_method (info, i))));

GIFunctionInfo * g_object_info_find_method (GIObjectInfo *info, const gchar *name);

# gint g_object_info_get_n_signals (GIObjectInfo *info);
# GISignalInfo * g_object_info_get_signal (GIObjectInfo *info, gint n);
void
g_object_info_get_signals (info)
	GIObjectInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_object_info_get_n_signals (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGISignalInfo (
		  g_object_info_get_signal (info, i))));

# gint g_object_info_get_n_vfuncs (GIObjectInfo *info);
# GIVFuncInfo * g_object_info_get_vfunc (GIObjectInfo *info, gint n);
void
g_object_info_get_vfuncs (info)
	GIObjectInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_object_info_get_n_vfuncs (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIVFuncInfo (
		  g_object_info_get_vfunc (info, i))));

# gint g_object_info_get_n_constants (GIObjectInfo *info);
# GIConstantInfo * g_object_info_get_constant (GIObjectInfo *info, gint n);
void
g_object_info_get_constants (info)
	GIObjectInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_object_info_get_n_constants (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIConstantInfo (
		  g_object_info_get_constant (info, i))));
