/*
 * Copyright (C) 2005 by the gtk2-perl team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: IdlArgInfo.xs,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $
 */

#include "gobject-introspection-perl.h"

SV *
newSVGIArgInfo (GIArgInfo *info)
{
        SV *sv = newSV (0);

        return sv_setref_pv (sv, "Glib::Object::Introspection::ArgInfo", info);
}

GIArgInfo *
SvGIArgInfo (SV *info)
{
        return INT2PTR (GIArgInfo *, SvIV (SvRV (info)));

}

MODULE = Glib::Object::Introspection::ArgInfo	PACKAGE = Glib::Object::Introspection::ArgInfo	PREFIX = g_arg_info_

GIDirection g_arg_info_get_direction (GIArgInfo *info);

gboolean g_arg_info_is_dipper (GIArgInfo *info);

gboolean g_arg_info_is_return_value (GIArgInfo *info);

gboolean g_arg_info_is_optional (GIArgInfo *info);

gboolean g_arg_info_may_be_null (GIArgInfo *info);

GITransfer g_arg_info_get_ownership_transfer (GIArgInfo *info);

GITypeInfo * g_arg_info_get_type (GIArgInfo *info);
