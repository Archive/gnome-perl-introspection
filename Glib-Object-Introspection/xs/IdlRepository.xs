/*
 * Copyright (C) 2005 by the gtk2-perl team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: IdlRepository.xs,v 1.2 2005/08/26 18:47:45 torsten Exp $
 */

#include "gobject-introspection-perl.h"
#include <dlfcn.h>
#include "ffi.h"

GType
gperl_i11n_get_gtype (GIRegisteredTypeInfo *info)
{
	return g_registered_type_info_get_g_type (info);
}

MODULE = Glib::Object::Introspection::Repository	PACKAGE = Glib::Object::Introspection::Repository	PREFIX = g_irepository_

# GIRepository * g_irepository_get_default (void);
GIRepository_noinc *
g_irepository_get_default (class)
    C_ARGS:
	/* void */

# void g_irepository_prepend_search_path (const char *directory);

# const char * g_irepository_load_typelib (GIRepository *repository, GTypelib *typelib, GIRepositoryLoadFlags flags, GError **error);

# gboolean g_irepository_is_registered (GIRepository *repository, const gchar *namespace, const gchar *version);

GIBaseInfo * g_irepository_find_by_name (GIRepository *repository, const gchar  *namespace, const gchar  *name);

=for apidoc __gerror__
=cut
# GTypelib * g_irepository_require (GIRepository *repository, const gchar *namespace, const gchar *version, GIRepositoryLoadFlags flags, GError **error);
GTypelib *
g_irepository_require (GIRepository *repository, const gchar *namespace, const gchar *version, GIRepositoryLoadFlags flags)
    PREINIT:
	GError *error = NULL;
    CODE:
	RETVAL = g_irepository_require (repository, namespace, version, flags, &error);
	if (error) {
		gperl_croak_gerror (NULL, error);
	}
    OUTPUT:
	RETVAL

# gchar ** g_irepository_get_dependencies (GIRepository *repository, const gchar *namespace);

# gchar ** g_irepository_get_loaded_namespaces (GIRepository *repository);
void
g_irepository_get_loaded_namespaces (repository)
	GIRepository *repository
    PREINIT:
	gchar **namespaces = NULL;
	gchar *namespace = NULL;
    PPCODE:
	namespaces = g_irepository_get_loaded_namespaces (repository);
	while ((namespace = *namespaces++)) {
		XPUSHs (sv_2mortal (newSVGChar (namespace)));
		g_free (namespace);
	}

# GIBaseInfo * g_irepository_find_by_gtype (GIRepository *repository, GType gtype);

# gint g_irepository_get_n_infos (GIRepository *repository, const gchar *namespace);
# GIBaseInfo * g_irepository_get_info (GIRepository *repository, const gchar *namespace, gint index);
void
g_irepository_get_infos (repository, namespace)
	GIRepository *repository
	const gchar *namespace
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_irepository_get_n_infos (repository, namespace);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIBaseInfo (
		  g_irepository_get_info (repository, namespace, i))));

# const gchar * g_irepository_get_typelib_path   (GIRepository *repository, const gchar  *namespace);

# const gchar * g_irepository_get_shared_library (GIRepository *repository, const gchar  *namespace);

# const gchar * g_irepository_get_version (GIRepository *repository, const gchar  *namespace);

# --------------------------------------------------------------------------- #

void
register_types (repository, namespace, package)
	GIRepository *repository
	const gchar *namespace
	const gchar *package
    PREINIT:
	gint number, i;
    CODE:
	number = g_irepository_get_n_infos (repository, namespace);
	for (i = 0; i < number; i++) {
		GIBaseInfo *info;
		GIInfoType info_type;
		const gchar *name;
		gchar *full_package;
		GType type;

		info = g_irepository_get_info (repository, namespace, i);
		info_type = g_base_info_get_type (info);
		name = g_base_info_get_name (info);

		if (info_type != GI_INFO_TYPE_OBJECT &&
		    info_type != GI_INFO_TYPE_BOXED &&
		    info_type != GI_INFO_TYPE_STRUCT &&
		    info_type != GI_INFO_TYPE_UNION &&
		    info_type != GI_INFO_TYPE_ENUM &&
		    info_type != GI_INFO_TYPE_FLAGS) {
			g_base_info_unref ((GIBaseInfo *) info);
			continue;
		}

		type = gperl_i11n_get_gtype ((GIRegisteredTypeInfo *) info);
		if (!type)
			croak ("Could not find GType for type %s::%s",
			       namespace, name);

		full_package = g_strconcat (package, "::", name, NULL);

		/* FIXME: This is a hack to get our AUTOLOAD involved. */
		if (info_type == GI_INFO_TYPE_OBJECT ||
		    info_type == GI_INFO_TYPE_BOXED ||
		    info_type == GI_INFO_TYPE_STRUCT ||
		    info_type == GI_INFO_TYPE_UNION) {
			gperl_set_isa (full_package, package);
		}

		switch (info_type) {
		    case GI_INFO_TYPE_OBJECT:
			gperl_register_object (type, full_package);
			break;

		    case GI_INFO_TYPE_BOXED:
		    case GI_INFO_TYPE_STRUCT:
		    case GI_INFO_TYPE_UNION:
			gperl_register_boxed (type, full_package, NULL);
			break;

		    case GI_INFO_TYPE_ENUM:
		    case GI_INFO_TYPE_FLAGS:
			gperl_register_fundamental (type, full_package);
			break;

		    default:
			break;
		}

		g_free (full_package);
		g_base_info_unref ((GIBaseInfo *) info);
	}
