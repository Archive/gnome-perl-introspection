/*
 * Copyright (C) 2005 by the gtk2-perl team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: IdlTypeInfo.xs,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $
 */

#include "gobject-introspection-perl.h"

SV *
newSVGITypeInfo (GITypeInfo *info)
{
        SV *sv = newSV (0);

        return sv_setref_pv (sv, "Glib::Object::Introspection::TypeInfo", info);
}

GITypeInfo *
SvGITypeInfo (SV *info)
{
        return INT2PTR (GITypeInfo *, SvIV (SvRV (info)));

}

MODULE = Glib::Object::Introspection::TypeInfo	PACKAGE = Glib::Object::Introspection::TypeInfo	PREFIX = g_type_info_

gboolean g_type_info_is_pointer (GITypeInfo *info);

GITypeTag g_type_info_get_tag (GITypeInfo *info);

GITypeInfo * g_type_info_get_param_type (GITypeInfo *info, gint n);

GIBaseInfo * g_type_info_get_interface (GITypeInfo *info);

gint g_type_info_get_array_length (GITypeInfo *info);

gboolean g_type_info_is_zero_terminated (GITypeInfo *info);

# gint g_type_info_get_n_error_domains (GITypeInfo *info);
# GIErrorDomainInfo * g_type_info_get_error_domain (GITypeInfo *info, gint n);
void
g_type_info_get_error_domains (info)
	GITypeInfo *info
    PREINIT:
	gint number, i;
    PPCODE:
	number = g_type_info_get_n_error_domains (info);
	EXTEND (sp, number);
	for (i = 0; i < number; i++)
		PUSHs (sv_2mortal (newSVGIErrorDomainInfo (
		  g_type_info_get_error_domain (info, i))));
