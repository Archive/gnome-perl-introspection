/*
 * Copyright (C) 2005 by the gtk2-perl team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id$
 */

#include "gobject-introspection-perl.h"

SV *
newSVGTypelib (GTypelib *typelib)
{
        SV *sv = newSV (0);

        return sv_setref_pv (sv, "Glib::Object::Introspection::Typelib", typelib);
}

GTypelib *
SvGTypelib (SV *typelib)
{
        return INT2PTR (GTypelib *, SvIV (SvRV (typelib)));

}

MODULE = Glib::Object::Introspection::Typelib	PACKAGE = Glib::Object::Introspection::Typelib	PREFIX = g_typelib_

# void g_typelib_free (GTypelib *typelib);
void
DESTROY (GTypelib *typelib)
    CODE:
	/* We don't own the typelib in most cases. */
	PERL_UNUSED_VAR (typelib);
	/* g_typelib_free (typelib); */

# GTypelib * g_typelib_new_from_memory (guchar *memory, gsize len);
# GTypelib * g_typelib_new_from_const_memory (const guchar *memory, gsize len);
# GTypelib * g_typelib_new_from_mapped_file (GMappedFile  *mfile);
GTypelib *
g_typelib_new_from_memory (class, SV *scalar)
    PREINIT:
	gchar *memory;
	STRLEN length;
    CODE:
	memory = SvPV (scalar, length);
	RETVAL = g_typelib_new_from_const_memory ((const guchar *) memory, length);
    OUTPUT:
	RETVAL

# gboolean g_typelib_symbol (GTypelib *typelib, const gchar *symbol_name, gpointer *symbol);

const gchar * g_typelib_get_namespace (GTypelib *typelib);
