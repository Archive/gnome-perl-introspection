/*
 * Copyright (C) 2005 by the gtk2-perl team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: gobject-introspection-perl.h,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $
 */

#ifndef _GOBJECT_INTROSPECTION_PERL_H_
#define _GOBJECT_INTROSPECTION_PERL_H_

#include <gperl.h>

#include <girepository.h>

#include "gobject-introspection-perl-types.h"
#include "gobject-introspection-perl-autogen.h"

SV * newSVGIBaseInfo (GIBaseInfo *info);
GIBaseInfo * SvGIBaseInfo (SV *info);

SV * newSVGIFunctionInfo (GIFunctionInfo *info);
GIFunctionInfo * SvGIFunctionInfo (SV *info);

SV * newSVGIStructInfo (GIStructInfo *info);
GIStructInfo * SvGIStructInfo (SV *info);

SV * newSVGIFieldInfo (GIFieldInfo *info);
GIFieldInfo * SvGIFieldInfo (SV *info);

SV * newSVGITypeInfo (GITypeInfo *info);
GITypeInfo * SvGITypeInfo (SV *info);

SV * newSVGIErrorDomainInfo (GIErrorDomainInfo *info);
GIErrorDomainInfo * SvGIErrorDomainInfo (SV *info);

SV * newSVGIInterfaceInfo (GIInterfaceInfo *info);
GIInterfaceInfo * SvGIInterfaceInfo (SV *info);

SV * newSVGIPropertyInfo (GIPropertyInfo *info);
GIPropertyInfo * SvGIPropertyInfo (SV *info);

SV * newSVGISignalInfo (GISignalInfo *info);
GISignalInfo * SvGISignalInfo (SV *info);

SV * newSVGIVFuncInfo (GIVFuncInfo *info);
GIVFuncInfo * SvGIVFuncInfo (SV *info);

SV * newSVGIConstantInfo (GIConstantInfo *info);
GIConstantInfo * SvGIConstantInfo (SV *info);

SV * newSVGICallableInfo (GICallableInfo *info);
GICallableInfo * SvGICallableInfo (SV *info);

SV * newSVGIArgInfo (GIArgInfo *info);
GIArgInfo * SvGIArgInfo (SV *info);

SV * newSVGIRegisteredTypeInfo (GIRegisteredTypeInfo *info);
GIRegisteredTypeInfo * SvGIRegisteredTypeInfo (SV *info);

SV * newSVGIValueInfo (GIValueInfo *info);
GIValueInfo * SvGIValueInfo (SV *info);

SV * newSVGIEnumInfo (GIEnumInfo *info);
GIEnumInfo * SvGIEnumInfo (SV *info);

SV * newSVGIObjectInfo (GIObjectInfo *info);
GIObjectInfo * SvGIObjectInfo (SV *info);

SV * newSVGTypelib (GTypelib *typelib);
GTypelib * SvGTypelib (SV *typelib);

#endif /* _GOBJECT_INTROSPECTION_PERL_H_ */
