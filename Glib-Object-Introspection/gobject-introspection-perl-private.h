/*
 * Copyright (C) 2005 by the gtk2-perl team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: gobject-introspection-perl.h,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $
 */

#ifndef _GOBJECT_INTROSPECTION_PERL_PRIVATE_H_
#define _GOBJECT_INTROSPECTION_PERL_PRIVATE_H_

#include "gobject-introspection-perl.h"

void * gperl_i11n_sv_to_pointer (GITypeInfo* info, SV *sv);
SV * gperl_i11n_pointer_to_sv (GITypeInfo* info, void* pointer, gboolean own);

GType gperl_i11n_get_gtype (GIRegisteredTypeInfo *info);

#endif /* _GOBJECT_INTROSPECTION_PERL_PRIVATE_H_ */
