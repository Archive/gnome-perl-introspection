#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 2;

# $Id: IdlValueInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $enum = $repository -> find_by_name("GObject", "TypeDebugFlags");
my @values = $enum -> get_values();
is(scalar @values, 4);
is($values[0] -> get_value(), 0);
