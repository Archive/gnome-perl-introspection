#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 13;

# $Id: IdlObjectInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $object = $repository -> find_by_name("GObject", "TypeModule");
isa_ok($object, "Glib::Object::Introspection::ObjectInfo");
isa_ok($object -> get_parent(), "Glib::Object::Introspection::ObjectInfo");

my @interfaces = $object -> get_interfaces();
is(scalar @interfaces, 1);
isa_ok($interfaces[0], "Glib::Object::Introspection::InterfaceInfo");

my @fields = $object -> get_fields();
is(scalar @fields, 5);
isa_ok($fields[0], "Glib::Object::Introspection::FieldInfo");

my @properties = $object -> get_properties();
is(scalar @properties, 0);

my @methods = $object -> get_methods();
is(scalar @methods, 7);
isa_ok($methods[0], "Glib::Object::Introspection::FunctionInfo");
isa_ok($object -> find_method("use"),
       "Glib::Object::Introspection::FunctionInfo");

my @signals = $object -> get_signals();
is(scalar @signals, 0);

my @vfuncs = $object -> get_vfuncs();
is(scalar @vfuncs, 0);

my @constants = $object -> get_constants();
is(scalar @constants, 0);
