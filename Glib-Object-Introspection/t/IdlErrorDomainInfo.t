#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 3;

# $Id: IdlErrorDomainInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

SKIP: {
  skip 'there is no error domain to test against', 3;

  my $domain = $repository -> find_by_name("Foo", "Errors1");
  isa_ok($domain, "Glib::Object::Introspection::ErrorDomainInfo");
  is($domain -> get_quark(), "foo_errors1_get_quark");
  isa_ok($domain -> get_codes(), "Glib::Object::Introspection::InterfaceInfo");
}
