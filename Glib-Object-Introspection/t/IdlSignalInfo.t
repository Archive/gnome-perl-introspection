#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 4;

# $Id: IdlSignalInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $object = $repository -> find_by_name("GObject", "Object");
my @signals = $object -> get_signals();
is(scalar @signals, 1);

my $signal = $signals[0];
ok($signal -> get_flags() >= [qw(run-last)]);
is($signal -> get_class_closure(), undef);
ok(!$signal -> true_stops_emit());
