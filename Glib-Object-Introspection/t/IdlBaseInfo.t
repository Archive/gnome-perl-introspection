#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 6;

# $Id: IdlBaseInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $info = $repository -> find_by_name("GObject", "Value");
isa_ok($info, "Glib::Object::Introspection::BaseInfo");

is($info -> get_name(), "Value");
is($info -> get_namespace(), "GObject");
ok(!$info -> is_deprecated());
is($info -> get_annotation("FIXME"), undef);

my $method = $info -> find_method("set_boxed");
isa_ok($method -> get_container(), "Glib::Object::Introspection::BaseInfo");
