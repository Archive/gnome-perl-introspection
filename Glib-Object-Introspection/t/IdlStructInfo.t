#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 6;

# $Id: IdlStructInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $struct = $repository -> find_by_name("GObject", "Value");
isa_ok($struct, "Glib::Object::Introspection::StructInfo");

my @fields = $struct -> get_fields();
is(@fields, 2);
isa_ok($fields[0], "Glib::Object::Introspection::FieldInfo");

my @methods = $struct -> get_methods();
is(@methods, 60);
isa_ok($methods[0], "Glib::Object::Introspection::FunctionInfo");
isa_ok($struct -> find_method("set_boxed"),
       "Glib::Object::Introspection::FunctionInfo");
