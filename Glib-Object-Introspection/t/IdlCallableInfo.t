#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 7;

# $Id: IdlCallableInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $interface = $repository -> find_by_name("GObject", "Value");

my $getter = $interface -> find_method("get_boxed");
isa_ok($getter, "Glib::Object::Introspection::CallableInfo");
isa_ok($getter -> get_return_type(), "Glib::Object::Introspection::TypeInfo");
ok($getter -> get_caller_owns());
ok(!$getter -> may_return_null());

my $setter = $interface -> find_method("set_boxed");
isa_ok($setter, "Glib::Object::Introspection::CallableInfo");
my @args = $setter -> get_args();
is(scalar @args, 1);
isa_ok($args[0], "Glib::Object::Introspection::ArgInfo");
