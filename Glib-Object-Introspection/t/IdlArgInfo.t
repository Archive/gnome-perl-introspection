#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 8;

# $Id: IdlArgInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $interface = $repository -> find_by_name("GObject", "Value");
my $method = $interface -> find_method("set_boxed");

my $arg = ($method -> get_args())[0];
isa_ok($arg, "Glib::Object::Introspection::ArgInfo");

is($arg -> get_direction(), "in");
ok(!$arg -> is_dipper());
ok(!$arg -> is_return_value());
ok(!$arg -> is_optional());
ok(!$arg -> may_be_null());
is($arg -> get_ownership_transfer(), "nothing");
isa_ok($arg -> get_type(), "Glib::Object::Introspection::TypeInfo");
