#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 9;

# $Id: IdlInterfaceInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $interface = $repository -> find_by_name("GObject", "TypePlugin");
isa_ok($interface, "Glib::Object::Introspection::InterfaceInfo");

my @prerequisites = $interface -> get_prerequisites();
is(scalar @prerequisites, 0);

my @properties = $interface -> get_properties();
is(scalar @properties, 0);

my @methods = $interface -> get_methods();
is(scalar @methods, 4);
isa_ok($methods[0], "Glib::Object::Introspection::FunctionInfo");
isa_ok($interface -> find_method("use"),
       "Glib::Object::Introspection::FunctionInfo");

my @signals = $interface -> get_signals();
is(scalar @signals, 0);

my @vfuncs = $interface -> get_vfuncs();
is(scalar @vfuncs, 0);

my @constants = $interface -> get_constants();
is(scalar @constants, 0);
