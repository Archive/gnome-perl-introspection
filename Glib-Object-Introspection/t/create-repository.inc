sub create_repository {
  my $repository = Glib::Object::Introspection::Repository -> get_default();
  $repository -> require('GObject', '2.0', []);
  $repository -> require('Gio', '2.0', []);
  return $repository;
}

1;
