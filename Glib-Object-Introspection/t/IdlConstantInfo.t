#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 2;

# $Id: IdlConstantInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $constant = $repository -> find_by_name("GObject", "PARAM_READWRITE");
isa_ok($constant, "Glib::Object::Introspection::ConstantInfo");
isa_ok($constant -> get_type(), "Glib::Object::Introspection::TypeInfo");
