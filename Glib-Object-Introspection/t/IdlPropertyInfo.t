#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 4;

# $Id: IdlPropertyInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $object = $repository -> find_by_name("Gio", "BufferedInputStream");

my @properties = $object -> get_properties();
is(scalar @properties, 1);

my $property = $properties[0];
isa_ok($property, "Glib::Object::Introspection::PropertyInfo");
ok($property -> get_flags() >= [qw/writable construct/]);
isa_ok($property -> get_type(), "Glib::Object::Introspection::TypeInfo");
