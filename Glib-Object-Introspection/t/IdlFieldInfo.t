#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 6;

# $Id: IdlFieldInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $struct = $repository -> find_by_name("GObject", "Value");

my @fields = $struct -> get_fields();
is(scalar @fields, 2);
isa_ok($fields[0], "Glib::Object::Introspection::FieldInfo");
ok($fields[0] -> get_flags() >= [qw(readable writable)]);
is($fields[0] -> get_size(), 0);
is($fields[0] -> get_offset(), 0);
isa_ok($fields[0] -> get_type(), "Glib::Object::Introspection::TypeInfo");
