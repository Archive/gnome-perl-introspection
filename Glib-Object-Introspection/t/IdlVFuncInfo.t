#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 3;

# $Id: IdlVFuncInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

SKIP: {
  skip 'there is no vfunc to test against', 3;

  my $object = $repository -> find_by_name("Foo", "Object1");
  my $vfunc = $object -> get_vfuncs();
  ok($vfunc -> get_flags() == []);
  is($vfunc -> get_offset(), 0);
  is($vfunc -> get_signal(), undef);
}
