#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 5;

# $Id: IdlFunctionInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $function = $repository -> find_by_name("GObject", "strdup_value_contents");
isa_ok($function, "Glib::Object::Introspection::FunctionInfo");
is($function -> get_symbol(), "g_strdup_value_contents");
ok($function -> get_flags() == []);

SKIP: {
  skip "FIXME: get_property() and get_vfunc() are broken", 2;
  is($function -> get_property(), undef);
  is($function -> get_vfunc(), undef);
}
