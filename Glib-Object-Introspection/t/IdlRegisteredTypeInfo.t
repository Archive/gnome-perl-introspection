#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 3;

# $Id: IdlRegisteredTypeInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $type = $repository -> find_by_name("GObject", "Value");
isa_ok($type, "Glib::Object::Introspection::RegisteredTypeInfo");
is($type -> get_type_name(), "GValue");
is($type -> get_type_init(), "g_value_get_type");
