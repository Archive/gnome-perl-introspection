#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 6;

# $Id: IdlRepository.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();
isa_ok($repository, "Glib::Object::Introspection::Repository");
isa_ok($repository, "Glib::Object");

my $info = $repository -> find_by_name("GObject", "Value");
isa_ok($info, "Glib::Object::Introspection::BaseInfo");

is_deeply([$repository -> get_loaded_namespaces()], [qw/Gio GLib GObject/]);

my @infos = $repository -> get_infos("GObject");
is(@infos, 290);
isa_ok($infos[0], "Glib::Object::Introspection::BaseInfo");
