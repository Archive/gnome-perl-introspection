#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 3;

# $Id: IdlEnumInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $enum = $repository -> find_by_name("GObject", "TypeDebugFlags");
isa_ok($enum, "Glib::Object::Introspection::EnumInfo");

my @values = $enum -> get_values();
is(scalar @values, 4);
isa_ok($values[0], "Glib::Object::Introspection::ValueInfo");
