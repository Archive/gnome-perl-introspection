#!/usr/bin/perl
use strict;
use warnings;
use Glib::Object::Introspection;
use Test::More tests => 7;

# $Id: IdlTypeInfo.t,v 1.1.1.1 2005/08/21 14:39:45 torsten Exp $

require 't/create-repository.inc';
my $repository = create_repository();

my $function = $repository -> find_by_name("GObject", "param_spec_pool_list_owned");
my $arg = ($function -> get_args())[0];
my $type = $arg -> get_type();

ok($type -> is_pointer());
is($type -> get_tag(), "interface");
is($type -> get_param_type(0), undef); # FIXME
is($type -> get_array_length(), -1);
ok(!$type -> is_zero_terminated());

isa_ok($type -> get_interface(), 'Glib::Object::Introspection::BaseInfo');
is($type -> get_error_domains(), undef);
