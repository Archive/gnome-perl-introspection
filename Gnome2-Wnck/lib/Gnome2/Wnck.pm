package Gnome2::Wnck;

=head1 NAME

Gnome2::Wnck - Perl interface to the Window Navigator Construction Kit

=head1 SYNOPSIS

  use Gnome2::Wnck;

  my $screen = Gnome2::Wnck::Screen -> get_default();
  $screen -> force_update();

  my $pager = Gnome2::Wnck::Pager -> new($screen);
  my $tasklist = Gnome2::Wnck::Tasklist -> new($screen);

=head1 ABSTRACT

This module allows a Perl developer to use the Window Navigator Construction
Kit library (libwnck for short) to write tasklists and pagers.

=head1 SEE ALSO

L<Gtk2>, L<Gtk2::api> and the source code of libwnck.

=head1 AUTHOR

Torsten Schoenfeld E<lt>kaffeetisch at gmx dot deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2003-2005 by the gtk2-perl team

=cut

# --------------------------------------------------------------------------- #

use warnings;
use strict;

use Glib::Object::Introspection;
use Gtk2;

use Carp qw(croak);

use base qw(Glib::Object::Introspection);

our $VERSION = '0.01';

sub import {
  Glib::Object::Introspection -> setup(Wnck => '2.24', 'Gnome2::Wnck');
}

# --------------------------------------------------------------------------- #

sub CHECK_VERSION {
  my $self = shift;

  eval "use ExtUtils::PkgConfig;";
  croak "Need ExtUtils::PkgConfig for version checking" if ($@);

  return ExtUtils::PkgConfig -> atleast_version("libwnck-1.0", join ".", @_);
}

# --------------------------------------------------------------------------- #

package Gnome2::Wnck::Application;

sub get {
  shift;
  return Glib::Object::Introspection::invoke(
    "Gnome2::Wnck::application_get", @_); # FIXME
}

sub get_windows {
  $Glib::Object::Introspection::AUTOLOAD =
    "Gnome2::Wnck::Application::get_windows";
  my $ref = Glib::Object::Introspection::AUTOLOAD(@_);

  if (wantarray()) {
    return defined $ref ? @$ref : ();
  } else {
    return defined $ref ? $ref->[-1] : undef;
  }
}

package Gnome2::Wnck::ClassGroup;

sub get {
  shift;
  return Glib::Object::Introspection::invoke(
    "Gnome2::Wnck::class_group_get", @_); # FIXME
}

sub get_windows {
  $Glib::Object::Introspection::AUTOLOAD =
    "Gnome2::Wnck::ClassGroup::get_windows";
  my $ref = Glib::Object::Introspection::AUTOLOAD(@_);

  if (wantarray()) {
    return defined $ref ? @$ref : ();
  } else {
    return defined $ref ? $ref->[-1] : undef;
  }
}

package Gnome2::Wnck::Pager;

sub new {
  $Glib::Object::Introspection::AUTOLOAD =
    "Gnome2::Wnck::Pager::new";
  return Glib::Object::Introspection::AUTOLOAD(@_);
}

package Gnome2::Wnck::Screen;

sub get {
  shift;
  return Glib::Object::Introspection::invoke(
    "Gnome2::Wnck::screen_get", @_); # FIXME
}

sub get_default {
  shift;
  return Glib::Object::Introspection::invoke(
    "Gnome2::Wnck::screen_get_default", @_); # FIXME
}

sub get_windows {
  $Glib::Object::Introspection::AUTOLOAD =
    "Gnome2::Wnck::Screen::get_windows";
  my $ref = Glib::Object::Introspection::AUTOLOAD(@_);

  if (wantarray()) {
    return defined $ref ? @$ref : ();
  } else {
    return defined $ref ? $ref->[-1] : undef;
  }
}

sub get_windows_stacked {
  $Glib::Object::Introspection::AUTOLOAD =
    "Gnome2::Wnck::Screen::get_windows_stacked";
  my $ref = Glib::Object::Introspection::AUTOLOAD(@_);

  if (wantarray()) {
    return defined $ref ? @$ref : ();
  } else {
    return defined $ref ? $ref->[-1] : undef;
  }
}

package Gnome2::Wnck::Tasklist;

sub new {
  $Glib::Object::Introspection::AUTOLOAD =
    "Gnome2::Wnck::Tasklist::new";
  return Glib::Object::Introspection::AUTOLOAD(@_);
}

package Gnome2::Wnck::Window;

sub get {
  shift;
  return Glib::Object::Introspection::invoke(
    "Gnome2::Wnck::window_get", @_); # FIXME
}

sub create_window_action_menu {
  return Glib::Object::Introspection::invoke(
    "Gnome2::Wnck::create_window_action_menu", @_);
}

1;
