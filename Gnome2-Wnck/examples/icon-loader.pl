#!/usr/bin/perl -w
use strict;
use Gtk2 -init;
use Gnome2::Wnck;

my $screen = Gnome2::Wnck::Screen->get_default;
$screen->force_update;

my $tasklist = Gnome2::Wnck::Tasklist->new ($screen);
$tasklist->set_screen ($screen);

$tasklist->set_icon_loader (sub {
  my ($icon_name, $size, $flags, $data) = @_;

  warn join ", ", @_;

  $icon_name = "gtk-ok";
  return Gtk2::IconTheme->get_default->load_icon ($icon_name, $size, []);
}, "bla");

my $window = Gtk2::Window->new;
$window->add ($tasklist);
$window->set_default_size (200, 100);

$window->signal_connect (destroy => sub { Gtk2->main_quit; });

$window->show_all;

Gtk2->main;
