#!/usr/bin/perl -w
use strict;
use Test::More;
use Gnome2::Wnck;

unless (Gtk2 -> init_check()) {
  plan skip_all => "Couldn't initialize Gtk2";
}
else {
  Gtk2 -> init();
  plan tests => 2;
}

my $selector = Gnome2::Wnck::Selector->new;
isa_ok($selector, 'Gnome2::Wnck::Selector');
isa_ok($selector, 'Gtk2::MenuBar');

__END__

###############################################################################

my $screen = Gnome2::Wnck::Screen -> get_default();
$screen -> force_update();

###############################################################################

my $workspace = $screen -> get_workspace(0);

SKIP: {
  skip("couldn't get first workspace", 7) unless (defined($workspace));

  is($workspace -> get_number(), 0);
  ok(defined($workspace -> get_name()));

 SKIP: {
    skip("get_width, get_height, get_viewport_x, get_viewport_y and is_virtual are new in 2.3.1", 5)
      unless (Gnome2::Wnck -> CHECK_VERSION(2, 4, 0));

    like($workspace -> get_width(), qr/^\d+$/);
    like($workspace -> get_height(), qr/^\d+$/);
    like($workspace -> get_viewport_x(), qr/^\d+$/);
    like($workspace -> get_viewport_y(), qr/^\d+$/);
    ok(not $workspace -> is_virtual());
  }

  if (Gnome2::Wnck -> CHECK_VERSION(2, 9, 91)) { # FIXME: 2.10
    $screen -> get_active_workspace() -> activate(time());
  }
  elsif (Gnome2::Wnck -> CHECK_VERSION(2, 0, 0)) {
    $screen -> get_active_workspace() -> activate();
  }

  # $workspace -> change_name(...);
}
